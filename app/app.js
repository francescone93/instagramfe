'use strict';

// Declare app level module which depends on views, and components
angular.module('instagram', [
    'ngRoute',
    'instagram.login',
    'instagram.registration',
    'instagramLogin',
    'instagramRegistration'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/login'});
}]).run(['$http', '$rootScope', function ($http, $rootScope) {
    $http.get('./config/url.json')
        .then(function (res) {
            $rootScope.base_api = res.data.production.base_api;
            console.log("Salvato l'url di base per api: " + $rootScope.base_api);
        })
        .catch(function (error) {
            console.log(error);
        });
}]);
