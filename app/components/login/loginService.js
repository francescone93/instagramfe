angular
    .module('instagramLogin', [])
    .factory('loginManager', loginManager);

loginManager.$inject = ['$http', '$rootScope'];

function loginManager($http, $rootScope) {
    var service = {};

    service.login = login;

    function login(email, psw) {
        // return Promise.reject({
        //     status: 400,
        //     data: {
        //         message: "Suca"
        //     }
        // })

        return $http.post($rootScope.base_api + 'login', {
           email: email,
           psw: psw
        });
    }

    return service;
    return service;
}

