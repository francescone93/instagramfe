'use strict';

angular.module('instagram.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'components/login/login.html',
        controller: 'loginController',
        controllerAs: 'login'
    });
}])

.controller('loginController', loginController);

loginController.$inject = ['loginManager'];

function loginController(loginManager) {
    var login = this;

    login.username = '';
    login.password = '';
    login.errorMsg = '';
    login.showError = false;

    login.doLogin = doLogin;

    function doLogin(username, password) {
        //TODO chiamata service
        loginManager.login(username, password)
          .then(function(data){
            login.username = data.username;
          })
          .catch(function(error){
            if (error.status === 400) {
              login.errorMsg = error.data.message;
              login.showConfirmation = false;
              login.showError = true;
          } else {
              login.errorMsg = "Si è verificato un errore durante il login";
              login.showConfirmation = false;
              login.showError = true;
          }
          })
        console.log(username, password)
    }
}