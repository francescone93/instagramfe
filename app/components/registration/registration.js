'use strict';

angular.module('instagram.registration', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/registration', {
        templateUrl: 'components/registration/registration.html',
        controller: 'registrationController',
        controllerAs: 'registration'
    });
}])

.controller('registrationController', registerController);

registerController.$inject = ['registrationManager', '$location', '$timeout'];

function registerController(registrationManager, $location, $timeout) {
    var registration = this;

    registration.email = '';
    registration.psw = '';
    registration.showConfirmation = false;
    registration.showError = false;
    registration.errorMsg = "";

    registration.doRegister = doRegister;

    function doRegister(email, psw) {
        registrationManager.register(email, psw)
            .then(function(res) {
                registration.email = '';
                registration.psw = '';

                registration.showConfirmation = true;
                registration.showError = false;

                $timeout(function() {
                    $location.path('/login');
                    $location.replace();
                }, 5000);
            })
            .catch(function(error) {
                if (error.status === 400) {
                    registration.errorMsg = error.data.message;
                    registration.showConfirmation = false;
                    registration.showError = true;
                } else {
                    registration.errorMsg = "Si è verificato un errore durante la registrazione";
                    registration.showConfirmation = false;
                    registration.showError = true;
                }
            });
    }
}